import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MynotesComponent } from './mynotes/mynotes.component';
import { SharednotesComponent } from './sharednotes/sharednotes.component';
import { NoteComponent } from './note/note.component';
import { NavbarComponent } from '../navbar/navbar.component';

@NgModule({
  declarations: [
    MynotesComponent,
    SharednotesComponent,
    NoteComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ]
})
export class NotesModule { }
