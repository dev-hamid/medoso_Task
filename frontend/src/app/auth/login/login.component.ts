import { Component, OnInit } from '@angular/core';
import User from '../../types/user';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  onSubmit() {
    this.auth.login(this.user);
  }

  constructor(private auth: AuthenticationService) { }
  ngOnInit() {
  }

}
