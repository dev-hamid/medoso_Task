import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import User from '../../types/user';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: User = new User();
  button = 'update user';

  async onSubmit() {
    if (this.router.url === '/updateuser') {
      await this.auth.updateUserData(this.user);
      this.user = this.auth.user;
    } else {
      await this.auth.register(this.user);
      this.user =  this.auth.user;
    }
  }

  async checkURL() {
    if (this.router.url === '/updateuser') {
      await this.auth.getUserData().then((res: User) => {
        this.user = res;
      });
    }
  }


  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.checkURL();
  }

}
