import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const token: string = this.auth.token;
    console.log(token, state.url);
    if (token) {
      if ((state.url === ('/register') || state.url === ('/login'))) {
        this.router.navigate(['/mynotes']);
      } else {
        return true;
      }
    } else {
      if ((state.url === ('/mynotes') || state.url === ('/sharednotes') || state.url === ('/updateuser') || state.url === ('/createnote') || state.url === ('/updatenote') || state.url === ('/'))) {
        this.router.navigate(['/login']);
      } else {
        return true;
      }
    }
  }

  constructor(private auth: AuthenticationService, private router: Router) { }
}
