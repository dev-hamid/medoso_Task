export default class User {
  id?: number;
  email: string;
  password: string;
  username?: string;
  fullname?: string;
  constructor() {
    this.email = '';
    this.password = '';
  }
}
